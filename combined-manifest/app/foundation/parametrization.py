from viktor.parametrization import Parametrization as ParametrizationBaseClass
from viktor.parametrization import Tab, Section, NumberField, DownloadButton


class Parametrization(ParametrizationBaseClass):
    geometry = Tab("Geometry")
    geometry.slab = Section("Slab")
    geometry.slab.width_x = NumberField("Width in x", suffix="mm", default=6000)
    geometry.slab.width_y = NumberField("Width in y", suffix="mm", default=5000)
    geometry.slab.thickness = NumberField("Thickness", suffix="mm", default=500)

    geometry.piles = Section("Piles")
    geometry.piles.diameter = NumberField("Diameter", suffix="mm", default=500)
    geometry.piles.length = NumberField("Length", suffix="m", default=7)

    loads = Tab("Loads")
    loads.input = Section("Input")
    loads.input.uniform_load = NumberField("Uniform load", suffix="kN/m2", default=1)

    scia = Tab("SCIA")
    scia.downloads = Section("Downloads")
    scia.downloads.input_xml_btn = DownloadButton("Input .xml", method="download_scia_input_xml")
    scia.downloads.input_def_btn = DownloadButton("Input .def", method="download_scia_input_def")
    scia.downloads.input_esa_btn = DownloadButton("Input .esa", method="download_scia_input_esa")
