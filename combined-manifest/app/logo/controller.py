from math import cos, sin, pi
from viktor import Color
from viktor.core import ViktorController
from viktor.geometry import Group
from viktor.views import DataGroup
from viktor.views import GeometryAndDataResult
from viktor.views import GeometryAndDataView
from viktor.views import Summary
from viktor.geometry import Line
from viktor.geometry import Point
from viktor.geometry import RectangularExtrusion
from viktor.views import DataItem

from .parametrization import Parametrization


class Controller(ViktorController):
    viktor_typed_empty_fields = True
    viktor_convert_date_field = True

    parametrization = Parametrization
    summary = Summary()

    @GeometryAndDataView('3D', duration_guess=1)
    def visualize(self, params, **kwargs):
        beam_length = params.input.geometry.beam_length

        # left beam
        x_end = beam_length * sin(20 * pi / 180)
        z_end = beam_length * cos(20 * pi / 180)
        point_start = Point(0, 0, 0)
        point_end = Point(-x_end, 0, z_end)
        _left_line = Line(point_start, point_end)
        left_beam = RectangularExtrusion(width=100, height=100, line=_left_line)

        # right beam
        point_end = Point(x_end, 0, z_end)
        _right_line = Line(point_start, point_end)
        right_beam = RectangularExtrusion(width=100, height=100, line=_right_line)

        # top beam
        point_start = Point(-x_end, 0, 1.1 * z_end)
        point_end = Point(x_end, 0, 1.1 * z_end)
        _top_line = Line(point_start, point_end)
        top_beam = RectangularExtrusion(width=100, height=100, line=_top_line)

        # bottom beam
        point_start = Point(-x_end, 0, -0.1 * z_end)
        point_end = Point(x_end, 0, -0.1 * z_end)
        _bottom_line = Line(point_start, point_end)
        bottom_beam = RectangularExtrusion(width=100, height=100, line=_bottom_line)

        geometries = [left_beam, right_beam, top_beam, bottom_beam]

        if params.input.visualization.color == 'Black':
            beam_color = Color(0, 0, 0)
        elif params.input.visualization.color == 'Blue':
            beam_color = Color(100, 130, 255)
        elif params.input.visualization.color == 'Yellow':
            beam_color = Color(255, 220, 0)
        elif params.input.visualization.color == 'Green':
            beam_color = Color(0, 255, 0)
        else:
            raise ValueError

        mass = 0
        for beam in geometries:
            beam.material.color = beam_color
            mass += beam.inner_volume * params.input.calculation.density * 1e-09  # inner volume in [mm3]

        data = DataGroup(DataItem(label='Mass', value=mass, suffix='kg'))

        return GeometryAndDataResult(Group(geometries), data)
