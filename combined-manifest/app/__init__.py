from .logo.controller import Controller as LogoController
from .calculation.controller import CalculationController
from .foundation.controller import Controller as FoundationController