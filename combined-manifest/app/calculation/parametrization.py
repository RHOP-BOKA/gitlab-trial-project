from viktor.parametrization import Parametrization, Tab, Section, NumberField, \
    LineBreak, ToggleButton, DownloadButton


class CalculationParametrization(Parametrization):

    general = Tab('General')
    general.beam = Section('Beam')
    general.beam.length = NumberField('Length (L)', suffix='mm', default=100)
    general.beam.width = NumberField('Width (W)', suffix='mm', default=10)
    general.beam.height = NumberField('Height (H)', suffix='mm', default=10)
    general.beam.E = NumberField('Modulus of Elasticity (E)', default=200000, suffix='N/mm2')

    general.loads = Section('Loads')
    general.loads.aw = NumberField('Starting point of load (aw)', suffix='mm', default=9)
    general.loads.nl = LineBreak()
    general.loads.wa = NumberField('Distributed load amplitude (wa)', suffix='N/mm', flex=40, default=5)
    general.loads.wL = NumberField('Distributed load amplitude (wL)', suffix='N/mm', flex=40, default=5)

    visualization = Tab('Visualization')
    visualization.axis_system = Section('Axis system')
    visualization.axis_system.show_axis_system = ToggleButton('Visualize axes', default=True)

    downloads = Tab('Downloads')
    downloads.calculation_sheet = Section('Calculation sheet')
    downloads.calculation_sheet.btn = DownloadButton('Download', 'download_spreadsheet')
