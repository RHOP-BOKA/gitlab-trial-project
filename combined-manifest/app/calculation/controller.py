from pathlib import Path

from viktor.core import ViktorController
from viktor.views import Summary
from viktor.views import DataResult
from viktor.views import DataGroup
from viktor.views import DataView
from viktor.views import DataItem
from viktor.views import SVGView
from viktor.views import SVGResult
from viktor.external.spreadsheet import SpreadsheetCalculationInput
from viktor.external.spreadsheet import SpreadsheetCalculation
from viktor.views import GeometryView
from viktor.views import GeometryResult
from viktor.geometry import Point
from viktor.geometry import CartesianAxes
from viktor.geometry import SquareBeam
from viktor.geometry import Group
from viktor.result import DownloadResult

from .parametrization import CalculationParametrization


class CalculationController(ViktorController):
    viktor_typed_empty_fields = True
    viktor_convert_date_field = True

    parametrization = CalculationParametrization
    summary = Summary()

    def get_evaluated_spreadsheet(self, params):
        inputs = [
            SpreadsheetCalculationInput('L', params['general']['beam']['length']),
            SpreadsheetCalculationInput('W', params['general']['beam']['width']),
            SpreadsheetCalculationInput('H', params['general']['beam']['height']),
            SpreadsheetCalculationInput('E', params['general']['beam']['E']),
            SpreadsheetCalculationInput('aw', params['general']['loads']['aw']),
            SpreadsheetCalculationInput('wa', params['general']['loads']['wa']),
            SpreadsheetCalculationInput('wL', params['general']['loads']['wL']),
        ]
        sheet_path = Path(__file__).parent / 'beam_calculation.xls'
        sheet = SpreadsheetCalculation.from_path(sheet_path, inputs=inputs)
        result = sheet.evaluate(include_filled_file=True)

        return result

    @DataView('Results', duration_guess=1)
    def get_data_view(self, params, **kwargs):
        result = self.get_evaluated_spreadsheet(params)

        max_deflection = result.values['maximum_deflection']
        max_bending_stress = result.values['maximum_bending_stress']
        data = DataGroup(
            maximum_deflection=DataItem('Maximum deflection', max_deflection, suffix='microns', number_of_decimals=2),
            maximum_bending_stress=DataItem('Maximum bending stress', max_bending_stress, suffix='N/mm2', number_of_decimals=2),
        )

        return DataResult(data)

    @SVGView('Schematic', duration_guess=1)
    def get_svg_view(self, params, **kwargs):
        img_path = Path(__file__).parent / 'beam_schematic.svg'
        return SVGResult.from_path(img_path)

    @GeometryView('3D', duration_guess=1)
    def get_3d_view(self, params, **kwargs):
        length_x = params['general']['beam']['length']
        length_y = params['general']['beam']['width']
        length_z = params['general']['beam']['height']

        beam = SquareBeam(length_x, length_y, length_z)

        axis_system_location = Point(- 0.5 * length_x - max(length_y, length_z) * 1.2, 0, 0)
        axis_system = CartesianAxes(axis_system_location, axis_length=5, axis_diameter=0.5)

        if params['visualization']['axis_system']['show_axis_system']:
            main_group = Group([beam, axis_system])
        else:
            main_group = Group([beam])

        return GeometryResult(main_group)

    def download_spreadsheet(self, params, **kwargs):
        result = self.get_evaluated_spreadsheet(params)
        return DownloadResult(result.file_content, 'evaluated_beam.xlsx')
