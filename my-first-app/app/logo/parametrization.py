from viktor.parametrization import Parametrization as ParametrizationBaseClass
from viktor.parametrization import Tab, Section, NumberField
from viktor.parametrization import OptionField, OptionListElement

color_options = [
    OptionListElement('Black'),
    OptionListElement('Blue'),
    OptionListElement('Yellow'),
    OptionListElement('Green')
]


class Parametrization(ParametrizationBaseClass):
    input = Tab('Input')
    input.geometry = Section('Geometry')
    input.geometry.beam_length = NumberField('Length', suffix='mm', default=2000)
    input.calculation = Section('Calculation')
    input.calculation.density = NumberField('Density', suffix='kg/m3', default=8050)
    input.visualization = Section('Visualization')
    input.visualization.color = OptionField("Color", options=color_options, default='Yellow')
